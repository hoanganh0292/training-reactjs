import React, { Component, useState } from "react";
import './styles.css';
interface RatingBarProps {
  max: number;
  ratingValue: number;
}

interface IRatingUnit {
  value: number;
  active: boolean;
}

interface RatingBarState {
  ratingUnits: IRatingUnit[];
  curRatingValue: number;
}

function calculate(max: number, ratingValue: number) {
    return Array.from({length: max},
      (_, index) => ({
        value: index + 1,
        active: index < ratingValue
      }));
  }

export function RatingBar (props: RatingBarProps) {
    // const units = calculate(props.max, props.ratingValue);
    // const [ratingUnits, setRatingUnits] = useState(units);
    const [ratingUnits, setRatingUnits] = useState(() => {
        const units = calculate(props.max, props.ratingValue);
        return units;
    });
    const [ratingValue, setRatingValue] = useState(props.ratingValue);
    
    const reset = () => {
        const ratingUnits = calculate(props.max, ratingValue);
        setRatingUnits(ratingUnits);
    }

  const select = (index: number) => {
    const ratingValue = index + 1;
    const units = ratingUnits.map((item, idx) => {
      return {
        ...item,
        active: item.active = idx <= index
      };
    });
    setRatingUnits(units);
    setRatingValue(ratingValue);
  }

  const enter = (index: number) => {
    const units = ratingUnits.map((item, idx) => {
      return {
        ...item,
        active: item.active = idx <= index
      };
    });
    setRatingUnits(units);
  }

    return (
        <>
        <h2>Rating Bar</h2>
        <div className="rating-bar" onMouseLeave={reset}>
            {
                ratingUnits.map((item, index) => (
                    <div key={item.value} className={`rating-unit ${item.active ? 'active' : ''}`}
                    onClick={() => select(index)} onMouseEnter={() => enter(index)}>
                    { item.value }
                    </div>
                ))
            }
        </div>
        </>
    );

  
}