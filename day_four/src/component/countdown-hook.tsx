import React, {Fragment, useState, useEffect, useRef } from "react";
  
export function Countdown (){    
    const [currentNumber, setCurretNumber] = useState(5);
    const [isStarted, setIsStarted] = useState(false);
    const [isDone, setIsDone] = useState(false); 
    const timer = useRef(0);        
    
    const startCountDown = () => {
        setIsStarted(true);
        const tempTimer = window.setInterval(() => {                                    
            setCurretNumber(t => {
                if (t <= 0)
                {                  
                    setIsDone(true);              
                    clearInterval(tempTimer);   
                    return 0;                 
                } 
                console.log(t);               
                return --t;
            });
            
        },1000);
        timer.current = tempTimer;
    }

    const pauseCountDown = () => {
        clearInterval(timer.current);
        setIsStarted(false);
    }

    const resetCountDown = () => {
        clearInterval(timer.current);               
        setCurretNumber(5);
        setIsDone(false);
        setIsStarted(false);
    }

    const countdownStyle = `
            .countdown-number {                 
            background-color: rgb(237,57,147);
            display: inline-block;        
            text-align: cener;            
            text-align: center;
            padding: 15px;
            color: white;
            margin: 5px;
            font-weight: bold;    
        }
        .button-control{
            margin-left: 5px
        }
        `
        return (
            <Fragment>
                <style>
                    {countdownStyle}
                </style>
                <div>
                    <button className='button-control' disabled={isStarted} onClick={() => startCountDown()}> Start </button>
                    <button className='button-control' onClick={() => pauseCountDown()}> Pause </button>
                    <button className='button-control' onClick={resetCountDown}> Reset </button>
                </div>
                <div className='countdown-number'> 
                    {currentNumber}                           
                </div>
                {isDone ? <div>Boomb!</div> : ''}                
            </Fragment>
        )
}