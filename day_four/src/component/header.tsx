import React, { Component } from "react";
import { User } from "../models/user";
import { UserContext, UCConsumer } from "../context/user-context";

interface HeaderProps {
  user: User;
}
export class Header extends Component {
    static contextType = UserContext;
    context!: React.ContextType<typeof UserContext>;
    render()
    {
        const {searchHistory = [], username} = this.context;
        return (
            <header className="header">
            <h2 className="title">Welcome {username}</h2>
            <div className="search">
                {searchHistory.map(item => <p>{item}</p>)}
            </div>
            <UserInfoMenu/>
            </header>
        );
    }
  
}

interface UserInfoMenuProps {
  user: User;
}

function UserInfoMenu() {  
  return (
      <UCConsumer>
          {
              (user) => {
                  return (
                    <nav className="header-nav">
                        <img src={user.avatarUrl} alt=""/>
                        <div>Dropdown menu below</div>
                    </nav>
                  );
              }
          }
      </UCConsumer>
    
  )
}