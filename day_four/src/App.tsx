import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { User } from './models/user';
import { Header } from './component/header';
import { SideNav } from './component/sidenav';
import { UCProvider } from './context/user-context';
import { RatingBar } from './component/rating-bar';
import { Countdown } from './component/countdown-hook';

interface AppState{
  user: User;
}

class App extends Component<{}, AppState>
{
  state: AppState = {
    user: {
      id: 'some-uuid',
      avatarUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Mr._Smiley_Face.svg/240px-Mr._Smiley_Face.svg.png',
      playlist: ['Microservices', 'TypeScript programming'],
      username: 'John Doe',
      searchHistory: [],
    }
  } 
  render(){
    const {user} = this.state;
    return(
      <UCProvider value={user}>
        <div className="container">
          <Header />
          <main>main content
            <div>
              Username {user.username}
            </div>            
            <RatingBar ratingValue={8} max={10} ></RatingBar>            
          </main>
          <aside>
            <SideNav user={user}/>
          </aside>
          
          <hr/>
        </div>
        <Countdown></Countdown>
      </UCProvider>
    );
  }
}

export default App;
