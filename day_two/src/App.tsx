import React, { Component } from 'react';
import './App.css';
// import { UserProfile } from './component/user-profile';
// import { Counter } from './component/counter';
// import { Countdown } from './component/countdown';
// import { NetWithState } from './component/online-offline';
import { NotificationSetting } from './component/notification-settings';

class App extends Component {
  state = {
      users : [
      { 
        userName: "Ironman",
        avatar: "https://media3.scdn.vn/img3/2019/2_6/mmFIO6_simg_de2fe0_500x500_maxb.jpg"
      },
      { 
        userName: 'Batman',
        avatar: 'https://my-test-11.slatic.net/p/d1aaf2e17ef046a2060c38414952a0fb.jpg_720x720q80.jpg_.webp'
      }
    ]
  };
  componentDidMount(){
    setTimeout(() => {
      const users = [...this.state.users, {
        userName: 'Wonder Woman',
        avatar: 'https://images-na.ssl-images-amazon.com/images/I/415IKuFkYoL.jpg'
      }];
      this.setState({
        users: users
      })
    }, 1000)
  }
  render (){    
    return (
      <div className="App">
        <header className="App-header">
          <h1>Training</h1>
        </header>      
        {/* 
        const {users} = this.state;
        { <UserProfile userName = "Anh Vu Hoang" avatar="https://cdn.iconscout.com/icon/free/png-512/avatar-380-456332.png" /> }
        {users.map(user => <UserProfile userName = {user.userName} avatar={user.avatar} />)} 
        <Counter></Counter>
        <Countdown></Countdown>        
        <NetWithState name={'vhanh'} ></NetWithState>
        */}
        <div style={{         
          margin: '5px 10px'
        }}>
          <NotificationSetting></NotificationSetting>
        </div>
      </div>
    );
  }

}

export default App;
