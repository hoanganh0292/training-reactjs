import React, { Component } from "react";

export class Counter extends Component <{}, {count: number}>
{
    state = {
        count : 1
    }
    //use arrow function to avoid lost this in callback function - onClick
    inc = () => {
        // this.setState({
        //     count: this.state.count + 1
        // })
         
        //Use arrow function to continue using previous state
        this.setState((preState) => {
            return {
                count: preState.count + 1
            }
        })         
        this.setState((preState) => {
            return {
                count: preState.count + 1
            }
        })       
    }

    dec = () => {
        this.setState({
            count: this.state.count - 1
        })        
    }

    render(){
        return (
            <div>
                <button onClick={this.dec}>-</button>
                <span>{this.state.count}</span>
                <button onClick={this.inc}>+</button>
            </div>
        )
    }
}