import React, {MouseEvent} from "react";

interface UserProfileProp{
    handleClick?: (e: MouseEvent) => {};
    userName: string;
    avatar?:string;
}
export function UserProfile(props: UserProfileProp){
    let element = <p>No Image</p>;
    if (props.avatar)
    {
        element = <img src={props.avatar} alt="" />
    }
    return (
        <div onClick={props.handleClick}>
            <h2>{props.userName}</h2>
            {element}
        </div>
    )
};