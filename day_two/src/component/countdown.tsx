import React, { Component, Fragment } from "react";

interface CountdownState {
    current: number;
    isDone:boolean;
    isStarted:boolean;
}
export class Countdown extends Component<{}, CountdownState>{
    state = {
        current: 5,
        isStarted: false,
        isDone: false
    };
    timer?: number;
    
    startCountDown = () => {           
        this.setState({
            isStarted : true
        })            
        const timer = window.setInterval(() => {
            if (this.state.current <= 0)
            {                  
                this.setState({
                    isDone : true
                })                 
                clearInterval(this.timer);
                return;
            }
            this.setState((preState) => {
                // if(this.state.current > 0)
                // {
                    
                // }
                return {
                    current : preState.current - 1              
                };                                                         
            })
        },1000);
        this.timer = timer;                                            
    }

    pauseCountDown = () => {
        if (this.timer)
        {
            clearInterval(this.timer);
        }
        this.setState({
            isStarted: false
        })
    }

    resetCountDown = () => {
        if (this.timer)
        {
            clearInterval(this.timer);
        }        
        this.setState({
            current: 5,
            isDone: false,
            isStarted:false
        })
    }

    render(){
        const countdownStyle = `
        .countdown-number {                 
            background-color: rgb(237,57,147);
            display: inline-block;        
            text-align: cener;            
            text-align: center;
            padding: 15px;
            color: white;
            margin: 5px;
            font-weight: bold;    
        }
        .button-control{
            margin-left: 5px
        }
        `
        return (
            <Fragment>
                <style>
                    {countdownStyle}
                </style>
                <div>
                    <button className='button-control' disabled={this.state.isStarted} onClick={this.startCountDown}> Start </button>
                    <button className='button-control' onClick={this.pauseCountDown}> Pause </button>
                    <button className='button-control' onClick={this.resetCountDown}> Reset </button>
                </div>
                <div className='countdown-number'> 
                    {this.state.current}                           
                </div>
                {this.state.isDone ? <div>Boomb!</div> : ''}                
            </Fragment>
        )
    }
}