import React, { Component } from 'react';
import './App.css';
import { NotificationSetting } from './component/notification-settings';
import { EditProfile } from './component/edit-profile';
import { ListCard } from './component/list-card';

class App extends Component { 
  render (){    
    return (
      <div className="App">
        <header className="App-header">
          <h1>Training</h1>
        </header>      
        
        <div style={{         
          display: 'flex'
        }}>
          {/* <NotificationSetting></NotificationSetting>
          <br/><br/><br/><br/>
          <EditProfile></EditProfile> */}
          <ListCard></ListCard>
        </div>
      </div>
    );
  }

}

export default App;
