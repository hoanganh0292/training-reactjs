import React, { Component } from "react";
import {Switch} from 'antd';

interface NotificationData{
    id: string;
    title:string;
    subTitle?:string;    
    status: boolean;
}

export class NotificationDetail extends Component<{
    noti: NotificationData, 
    onChange: (checked: boolean, noti: NotificationData) => void}, {}>
{
    handleSwitchChange = (checked: boolean) => 
    {
        this.props.onChange(checked, this.props.noti);
    }
    render()
    {
        const {noti} = this.props;
        return (
            <div>
                <h3>{noti.title}</h3>
                {noti ? <h4>{noti.subTitle}</h4> : null}
                <Switch checked={noti.status} onChange={this.handleSwitchChange}/>                        
            </div>
        )
    }
}

const NotiMemo = React.memo(NotificationDetail);

export class NotificationSetting extends Component <{}, {
    notifications: Array<NotificationData>
}>
{    
    state = {
        notifications: [
            {
                id: 'email',
                title: 'Email notification',
                subTitle: 'Something',
                status: true
            },
            {
                id: 'push',
                title: 'Push notification',
                status: false
            },
            {
                id: 'monthly-reports',
                title: 'Mothly notification',
                status: false
            },
            {
                id: 'quarter',
                title: 'Quarter notification',
                status: false
            }
        ]
    }
    
    handleSwitchChange = (checked:boolean, noti: NotificationData) =>
    {
        console.log(checked, noti.id);
        const notisNew =  this.state.notifications.map (n => {
            if (noti.id === n.id)
            {
                return{
                    ...n,
                    status: checked
                }
            }
            return n;
        })
        this.setState({
            notifications: notisNew
        })
    }

    render()
    {
        const {notifications} = this.state;
        return(
            <>
            {notifications.map((noti) => {
                return (
                    <NotiMemo key={noti.id} noti={noti} onChange={this.handleSwitchChange}/>                   
                )
            })}
            
            </>
        )
    }    
}

