import React, { Component } from "react";
import {Card } from 'antd';
import {CheckCircleTwoTone, EnvironmentOutlined } from '@ant-design/icons';

interface CardData{
    id: string;
    type: 'location' | 'tick';
    distance: number;
    price: number;    
    description?: string;
}

export class CardDetail extends Component<{
    card: CardData
}, {}>
{    
    render(){
        const {card} = this.props;
        return (
            <div>
                <Card style={{ width: 180, margin: 10, background:'#d1e9e5'}}>                                                       
                            {card.type === 'location' ? <EnvironmentOutlined twoToneColor="#87d5e2" style={{fontSize: 32}} /> 
                                                      : <CheckCircleTwoTone twoToneColor="#52c41a" style={{fontSize: 32}} />}
                            {card.type === 'location' ? <p className='card-main-text'>{card.distance.toLocaleString('en')} mi</p>
                                                      : <p className='card-main-text'>${card.price.toLocaleString('en')}</p>}                        
                            <p>{card.description} {card.id}</p>
                </Card >                        
            </div>
        )
    }
}

const CardDetailMemo = React.memo(CardDetail);

export class ListCard extends Component<{}, {
    arrCard: Array<CardData>
}>
{
    state = {
        arrCard: [
            {
                id: '01',
                type: 'location' as 'location',
                distance: 158.3,    
                price: 0,            
                description: 'Distance driven'
            },            
            {
                id: '03',
                type: 'location' as 'location',
                distance: 123.5,    
                price: 0,            
                description: 'Distance driven'
            },
            {
                id: '04',
                type: 'location' as 'location',
                distance: 2345.5,    
                price: 0,            
                description: 'Distance driven'
            },
            {
                id: '02',
                type: 'tick' as 'tick',
                distance: 0,
                price: 1428,
                description: 'Vehicles on track'
            },
            {
                id: '05',
                type: 'tick' as 'tick',
                distance: 0,
                price: 2020,
                description: 'Vehicles on track'
            },
            {
                id: '06',
                type: 'tick' as 'tick',
                distance: 0,
                price: 10293,
                description: 'Vehicles on track'
            }
        ]
    }
    componentDidMount(){
        window.setInterval(() => {
            const newCards = this.state.arrCard.map(card => {
                return {
                    ...card,
                    price: Math.floor(Math.random() * 10000),
                    distance: Math.random() * 1000
                }
            });
            this.setState({
                arrCard: newCards
            })
          }, 5000)          
    }
    render(){
        const {arrCard} = this.state;
        return (
            <>
            {
                arrCard.map((card) => {
                    return (
                        <CardDetailMemo key={card.id} card={card}/>                   
                    )
                })
            }
            </>
        );
    }
}