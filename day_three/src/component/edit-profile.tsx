import React, { Component } from "react";

export class EditProfile extends Component<{}, {
    form: {
        userName: string;
        email: string;
    }
}>
{
    state= {
        form : {
            userName: 'React Core',
            email: 'abc@gmail.com'
        }
    }
    handleSubmit(){}

    onNameChange = (e: React.ChangeEvent) => {
        const value = (e.target as HTMLInputElement).value;
        this.setState(preState => {
            return {
                form: {
                ...preState.form,
                userName: value
                }
            }
        });
    }
    render()
    {
        const {form} = this.state;
        return (
            <form onSubmit={this.handleSubmit}>
                <input type="text" value={form.userName} onChange={this.onNameChange}></input>
                <br/>
                <input type="text" value={form.email} readOnly></input>
                <br/>
                <button type="submit">Save</button>
            </form>
        )
    }

}