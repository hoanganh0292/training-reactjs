import React, { Component } from "react";

interface OnlineOfflineState{
    status: 'online' | 'offline';
}

export function withNetworkStatus(OtherComponent: any)
{
    return class OnlineOffline extends Component<any, OnlineOfflineState>{
        state = {
            status: 'online' as 'online'
        }
    
        updateState = (e : Event) => {
            this.setState({
              status: e.type === 'online' ? 'online' : 'offline'  
            })
        }
        componentDidMount() {
            window.addEventListener('online', this.updateState);
            window.addEventListener('offline', this.updateState);
        }
        componentWillUnmount(){
            window.removeEventListener('online', this.updateState);
            window.removeEventListener('offline', this.updateState);
        }
    
        render() {
            return (            
                // status will auto map in Net props and display in result   
                <OtherComponent status= {this.state.status} {...this.props}/>
            );
        }
    
    }
}

export function Net(props: {status: OnlineOfflineState, name:string})
{
    return (
        <>
            <div>Net status: {props.status}</div>
            <div>Net name: {props.name}</div>
        </>
    )
}
export const NetWithState = withNetworkStatus(Net);

