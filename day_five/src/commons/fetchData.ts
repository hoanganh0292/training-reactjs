//Consider using library such as axios to handle error
export async function fetchData(input: RequestInfo, init?: RequestInit | undefined){
    const response = await fetch(input, init);
    if (response.status < 200 || response.status >= 300){
        throw response;
    }    
    console.log(response);
    return response;
}