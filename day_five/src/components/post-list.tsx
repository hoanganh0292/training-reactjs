import React, { useEffect, useReducer } from 'react';
import { fetchData } from '../commons/fetchData';
import { useRouteMatch, Link, Route } from 'react-router-dom';
import { PostDetail } from './post-detail';
import { Post } from '../model/post';

interface PostListState{
    posts: Post[];
    loading: boolean;
}

interface ActionTypes{
    readonly type: 'GET_POST_LIST' | 'GET_POST_LIST_SUCCESS' | 'GET_POST_LIST_FAILED';
    payload?: Post[];
}

function reducer(state: PostListState, action: ActionTypes)
{
    switch (action.type){
        case 'GET_POST_LIST':
            return{
                ...state,
                loading: true
            };
        case 'GET_POST_LIST_SUCCESS':
            return{
                ...state,
                loading: false,
                posts: action.payload!
            };
        case 'GET_POST_LIST_FAILED':
            return{
                ...state,
                loading: false
            };
        default:
            throw new Error('Unknow action');
    }
    
}

export default function PostList(){
    const [state, dispatch] = useReducer(reducer, {
        posts: [],
        loading: false
    });
    const {posts, loading} = state;
    const match = useRouteMatch();
    // const [post, setPosts] = useState<Post[]>([]);
    // const [loading, setLoading] = useState(false);

    useEffect(() => {
        //setLoading(true);
        dispatch({
            type: 'GET_POST_LIST'
        });
        (async () => {
            try {
                const response = await fetchData('http://localhost:4000/posts');
                const data = await response.json() as Post[];
                //setPosts(data);
                console.log(data);
                dispatch({
                    type: 'GET_POST_LIST_SUCCESS',
                    payload: data
                });
            } 
            catch (error) {
                dispatch({
                    type: 'GET_POST_LIST_FAILED'
                });
            }
            // finally{
            //     setLoading(false);
            // }
        })();
    }, []);
    return (
        <div>
            {loading ? <p>Loading...</p> 
                     : <div>
                        {
                            posts.map(post => <div key={post.id}>
                             <Link to = {`${match.path}/${post.id}`}>
                                {post.id}. {post.title} - {post.author}
                             </Link>
                             </div>)
                        }
                       </div>}
            <switch>
                <Route path={`${match.path}/:postId`}>
                    <PostDetail></PostDetail>
                </Route>
            </switch>
        </div>
    );

}
