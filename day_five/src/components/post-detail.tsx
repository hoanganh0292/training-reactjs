import React, { useEffect, useReducer } from "react";
import { useParams } from "react-router-dom";
import { Post } from "../model/post";
import { fetchData } from "../commons/fetchData";


interface PostDetailState{
    post: Post | null;
    loading: boolean;
}

interface ActionTypes{
    readonly type: 'GET_POST' | 'GET_POST_SUCCESS' | 'GET_POST_FAILED';
    payload?: Post;
}

function reducer(state: PostDetailState, action: ActionTypes)
{
    switch (action.type){
        case 'GET_POST':
            return{
                ...state,
                loading: true
            };
        case 'GET_POST_SUCCESS':
            return{
                ...state,
                loading: false,
                post: action.payload!
            };
        case 'GET_POST_FAILED':
            return{
                ...state,
                loading: false
            };
        default:
            throw new Error('Unknow action');
    }
    
}


export function PostDetail() {
    const {postId} =useParams();
    const [state, dispatch] = useReducer(reducer, {
        post: null,
        loading: false
    });
    const {post, loading} = state;
    useEffect(() => {        
        dispatch({
            type: 'GET_POST'
        });
        (async () => {
            try {
                const response = await fetchData('http://localhost:4000/posts/' + postId);
                const data = await response.json() as Post;
                console.log(data);
                dispatch({
                    type: 'GET_POST_SUCCESS',
                    payload: data
                });
            } 
            catch (error) {
                dispatch({
                    type: 'GET_POST_FAILED'
                });
            }            
        })();
    }, [postId]);
    return (
        <>
            <div>Detail with postId: {postId}</div>
            {loading ? <p>Loading...</p>
                : <div>
                    <h3>{post?.title}</h3>
                    <div>{post?.body}</div>
                </div>
            }
        </>
    )
} 