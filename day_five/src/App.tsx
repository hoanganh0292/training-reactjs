import React, { Suspense } from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { Profile } from './components/profile';
const PostList = React.lazy(() => import('./components/post-list'));

function App() {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <div className="App">
        <Router>
          <div>
            <nav>
              <ul>
                <li>
                  <Link to="/">Home</Link>
                </li>
                <li>
                  <Link to="/profile">Profile</Link>
                </li>
                <li>
                  <Link to="/posts">Posts</Link>
                </li>
              </ul>
            </nav>

            {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
            <Switch>
              <Route path="/profile">
                <Profile />
              </Route>
              <Route path="/posts">
                <PostList />
              </Route>
            </Switch>
          </div>
        </Router>
      </div>
    </Suspense>
  );
}

export default App;
