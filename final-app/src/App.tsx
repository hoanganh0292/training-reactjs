import React, { Suspense } from 'react';
import './App.css';
import { Layout, Menu } from 'antd';
import { AppstoreOutlined, LineChartOutlined, MailOutlined, SettingOutlined } from '@ant-design/icons';
import {
  BrowserRouter as Router, Route, Link, Switch, Redirect
} from "react-router-dom";
import { RequestsInfo } from './components/request-info';
import { SettingComponent } from './components/temp-component/setting';
import { EmailComponent } from './components/temp-component/email';
import { StaticComponent } from './components/temp-component/static';
import { EditProfile } from './components/edit-profile';
const { Header, Content, Sider } = Layout;

function App() {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <div className="App">
        <Router>
          <Layout>
            <Sider width={75} theme="light">
              <div className='logo'></div>
              <Menu mode="inline" defaultSelectedKeys={['1']}>
                <Menu.Item key="1" icon={<AppstoreOutlined />}>
                  <Link to="/requests"></Link>
                </Menu.Item>
                <Menu.Item key="2" icon={<LineChartOutlined />}>
                  <Link to="/static"></Link>
                </Menu.Item>
                <Menu.Item key="3" icon={<MailOutlined />}>
                  <Link to="/email"></Link>
                </Menu.Item>
                <Menu.Item key="4" icon={<SettingOutlined />}>
                  <Link to="/settings"></Link>
                </Menu.Item>
              </Menu>
            </Sider>
            <Layout className='site-layout'>
              <Header className='header-layout' style={{ padding: 0, height: 50 }}>
              </Header>
              <Content className="main-content" >
                <Switch>                                 
                  <Route path="/requests">
                    <RequestsInfo></RequestsInfo>
                  </Route>
                  <Route path="/profiles/:profileId">
                    <EditProfile></EditProfile>
                  </Route>
                  <Route path="/static">
                    <StaticComponent></StaticComponent>
                  </Route>
                  <Route path="/email">
                    <EmailComponent></EmailComponent>
                  </Route>
                  <Route path="/settings">
                    <SettingComponent></SettingComponent>
                  </Route>
                  <Route path="/" render={() => (<Redirect to="/requests" />)} >                    
                  </Route>
                </Switch>
              </Content>
            </Layout>
          </Layout>
        </Router>
      </div>
    </Suspense>
  );
}

export default App;
