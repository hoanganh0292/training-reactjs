export interface CardInfo{
    resolvedTicket: number,
    timeAverage: number,
    medianNPS: number    
}