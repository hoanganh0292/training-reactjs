export interface RequestDetail{
    id: string,
    profileId: string,
    avatar: string,
    name: string,
    title: string,
    role: string,
    age: number,
    email: string,
    phone: string,
    attendant: string
}