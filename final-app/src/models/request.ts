export interface Request{
    id: string,
    subject: string,
    requestDate: Date,
    latestUpdate: Date,
    statusName: string
}