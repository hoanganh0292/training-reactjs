
export interface Profile{
    name: string,
    titleValue: number,
    roleValue: number,
    birthday: Date,
    experience: number,
    email: string,
    phone: string,
    address:string

}