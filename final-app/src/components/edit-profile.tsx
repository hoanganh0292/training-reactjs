import React, { useReducer, useEffect } from "react"
import { Form, Button, Input, DatePicker, InputNumber, Select, Space, notification } from "antd"
import { useParams } from "react-router-dom";
import { Profile } from "../models/profile";
import { fetchData } from "../commons/fetch-data";
import moment from "moment";


interface ProfileDetailState {
    profile: Profile | null;
    loading: boolean;
}

interface ActionTypes {
    readonly type: 'GET_PROFILE' | 'GET_PROFILE_SUCCESS' | 'GET_PROFILE_FAILED';
    payload?: Profile;
}

function reducer(state: ProfileDetailState, action: ActionTypes) {
    switch (action.type) {
        case 'GET_PROFILE':
            return {
                ...state,
                loading: true
            };
        case 'GET_PROFILE_SUCCESS':
            return {
                ...state,
                loading: false,
                profile: action.payload!
            };
        case 'GET_PROFILE_FAILED':
            return {
                ...state,
                loading: false
            };
        default:
            throw new Error('Unknow action');
    }

}
export function EditProfile() {
    const { profileId } = useParams();
    const [state, dispatch] = useReducer(reducer, {
        profile: null,
        loading: false
    });
    const { profile, loading } = state;

    useEffect(() => {
        dispatch({
            type: 'GET_PROFILE'
        });
        (async () => {
            try {
                const response = await fetchData('http://localhost:4010/profiles/' + profileId);
                const data = await response.json() as Profile;
                //console.log(data);
                dispatch({
                    type: 'GET_PROFILE_SUCCESS',
                    payload: data
                });
            }
            catch (error) {
                dispatch({
                    type: 'GET_PROFILE_FAILED'
                });
            }
        })();
    }, [profileId]);
    const layout = {
        labelCol: {
            span: 4,
        },
        wrapperCol: {
            span: 14,
        },
    };
    const tailLayout = {
        wrapperCol: {
            offset: 4,
            span: 14,
        },
    };
    const handleSubmit = () => {
        openNotification();
    }
    const openNotification = () => {
        notification.open({
          message: 'Comming soon',
          description:
            'Application is using json-db server, feature will complete soon.',
          onClick: () => {
            console.log('Notification Clicked!');
          },
        });
      };
    return (
        <>
            {
                loading ? <p>Loading...</p>
                    : <div className="profile-edit">
                        <Form {...layout}
                            initialValues={{
                                size: "large",
                            }}
                            layout="horizontal"
                            size={"large"}
                        >
                            <Form.Item label="Name">
                                <Input value={profile?.name} />
                            </Form.Item>
                            <Form.Item label="Title">
                                <Select value ={profile?.titleValue}>
                                    <Select.Option value="0">Junior</Select.Option>
                                    <Select.Option value="1">Senior</Select.Option>
                                    <Select.Option value="2">Manager</Select.Option>
                                </Select>
                            </Form.Item>
                            <Form.Item label="Role">
                                <Select value ={profile?.roleValue}>
                                    <Select.Option value="0">Guest</Select.Option>
                                    <Select.Option value="1">Admin</Select.Option>
                                </Select>
                            </Form.Item>
                            <Form.Item label="Birthday">
                                <DatePicker format="DD/MM/YYYY" defaultValue={moment(profile?.birthday,'MM-DD-YYYY')} />
                            </Form.Item>

                            <Form.Item label="Experience">
                                <InputNumber value={profile?.experience} />
                            </Form.Item>
                            <Form.Item label="Email">
                                <Input value={profile?.email}/>
                            </Form.Item>
                            <Form.Item label="Phone">
                                <Input value={profile?.phone}/>
                            </Form.Item>
                            <Form.Item label="Address">
                                <Input value={profile?.address}/>
                            </Form.Item>
                            <Form.Item {...tailLayout}>
                                <Space>
                                    <Button type="primary" htmlType="submit" onClick={handleSubmit}>Submit</Button>
                                    <Button htmlType="button" href="/requests">Cancel</Button>
                                </Space>
                            </Form.Item>
                        </Form>
                    </div>

            }
        </>

    );
}
