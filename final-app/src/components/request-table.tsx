import React, { useEffect, useReducer, useState } from 'react';
import { fetchData } from '../commons/fetch-data';
import { Table } from 'antd';
import { useRouteMatch, Link, useLocation  } from 'react-router-dom';
import { Request } from '../models/request';

interface RequestListState {
    requests: Request[];
    loading: boolean;
}

interface ActionTypes {
    readonly type: 'GET_REQUEST_LIST' | 'GET_REQUEST_LIST_SUCCESS' | 'GET_REQUEST_LIST_FAILED';
    payload?: Request[];
}

function reducer(state: RequestListState, action: ActionTypes) {
    switch (action.type) {
        case 'GET_REQUEST_LIST':
            return {
                ...state,
                loading: true
            };
        case 'GET_REQUEST_LIST_SUCCESS':
            return {
                ...state,
                loading: false,
                requests: action.payload!
            };
        case 'GET_REQUEST_LIST_FAILED':
            return {
                ...state,
                loading: false
            };
        default:
            throw new Error('Unknow action');
    }

}

export default function RequestTable() {
    const [state, dispatch] = useReducer(reducer, {
        requests: [],
        loading: false,
    });
    const path = useLocation().pathname;
    const { requests, loading } = state;
    
    const[activeRow, setActiveRow] = useState(path.startsWith('/requests/') ? path.substr(path.lastIndexOf('/')+1,path.length) : "");
    const match = useRouteMatch();  
    console.log(activeRow);
    
    useEffect(() => {
        dispatch({
            type: 'GET_REQUEST_LIST'
        });
        (async () => {
            try {
                const response = await fetchData('http://localhost:4010/requests');
                const data = await response.json() as Request[];
                //console.log(data);
                dispatch({
                    type: 'GET_REQUEST_LIST_SUCCESS',
                    payload: data
                });
            }
            catch (error) {
                dispatch({
                    type: 'GET_REQUEST_LIST_FAILED'
                });
            }
        })();
    }, []);
    

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            render: (text: string) => (
                <b>#{text}</b>
            ),
        },
        {
            title: 'SUBJECT',
            dataIndex: 'subject',
            key: 'subject'
        },
        {
            title: 'REQUEST DATE',
            dataIndex: 'requestDate',
            key: 'requestDate',
        },
        {
            title: 'LATEST UPDATE',
            dataIndex: 'latestUpdate',
            key: 'latestUpdate',
        },
        {
            title: 'STATUS',
            key: 'statusName',
            dataIndex: 'statusName',
        },
        {
            title: 'Action',
            key: 'action',            
            render: (text: string, request: Request) => (
                <Link to={`${match.path}/${request.id}`} onClick={() => hello(request.id)}>
                    Open
                </Link>
            ),
        },
    ];
    const hello = (id:string) => {
        setActiveRow(id);
    }
    const setRowClassName = (record:Request) => {
        return record.id === activeRow ? 'clickRowStyle' : '';
      }
    return (
        <div className="request-table">
            {
                loading ? <p>Loading...</p>
                    : <Table rowKey="id" columns={columns} 
                        dataSource={requests} 
                        pagination= {false}      
                        rowClassName={setRowClassName}                  
                    />
            }            
        </div>
    );

}
