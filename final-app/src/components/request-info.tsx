import React from "react";
import RequestTable from "./request-table";
import { Route, useRouteMatch, Switch } from "react-router-dom";
import { RequestDetailInfo } from "./request-detail";
import { CardItems } from "./card-item";


export function RequestsInfo() {
    const match = useRouteMatch(); 
    return (
        <>
            <div className="request">
                <CardItems></CardItems>
                <RequestTable></RequestTable>
            </div>
            <div className="customer-detail">
            <Switch>
                <Route path={`${match.path}/:requestId`}>
                    <RequestDetailInfo></RequestDetailInfo>
                </Route>
            </Switch>
            </div>
        </>
    )
}