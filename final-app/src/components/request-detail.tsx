import React, { useEffect, useReducer } from "react";
import { useParams } from "react-router-dom";
import { RequestDetail } from "../models/requestDetail";
import { fetchData } from "../commons/fetch-data";
import { Avatar, Button, Space } from "antd";
import { UserOutlined } from '@ant-design/icons';

interface RequestDetailState{
    customer: RequestDetail | null;
    loading: boolean;
}

interface ActionTypes{
    readonly type: 'GET_DETAIL' | 'GET_DETAIL_SUCCESS' | 'GET_DETAIL_FAILED';
    payload?: RequestDetail;
}

function reducer(state: RequestDetailState, action: ActionTypes)
{
    switch (action.type){
        case 'GET_DETAIL':
            return{
                ...state,
                loading: true
            };
        case 'GET_DETAIL_SUCCESS':
            return{
                ...state,
                loading: false,
                customer: action.payload!
            };
        case 'GET_DETAIL_FAILED':
            return{
                ...state,
                loading: false
            };
        default:
            throw new Error('Unknow action');
    }

}


export function RequestDetailInfo() {
    const {requestId} =useParams();
    const [state, dispatch] = useReducer(reducer, {
        customer: null,
        loading: false
    });
    
    const detailElement = document.getElementsByClassName("customer-detail");
    detailElement.length  > 0 && ((detailElement[0] as  HTMLElement).style.display='block');
    
    const {customer, loading} = state;
    useEffect(() => {
        dispatch({
            type: 'GET_DETAIL'
        });
        (async () => {
            try {
                const response = await fetchData('http://localhost:4010/requestDetail/' + requestId);
                const data = await response.json() as RequestDetail;
                //console.log(data);
                dispatch({
                    type: 'GET_DETAIL_SUCCESS',
                    payload: data
                });
            }
            catch (error) {
                dispatch({
                    type: 'GET_DETAIL_FAILED'
                });
            }
        })();
    }, [requestId]);
    return (
        <>
            {loading ? <p>Loading...</p>
                : <div>
                    <div className="customer-common-info">
                    <Avatar size={150} icon={<UserOutlined />} src={customer?.avatar} />
                    <h3>{customer?.name}</h3>
                    <div>{customer?.title}</div>
                    <br/>
                    <Space size="middle">
                        <Button type="primary" size="large" href={"/profiles/" + customer?.profileId} >Edit Profile</Button>
                        <Button type="default" size="large" href={"/profiles/" + customer?.profileId}>Change Status</Button>
                    </Space>
                    </div>
                    <hr></hr>
                    <div>
                    <label>Role</label><br/>
                    <h4>{customer?.role}</h4><br/>

                    <label>Age</label><br/>
                    <h4>{customer?.age}</h4><br/>

                    <label>Email</label><br/>
                    <h4>{customer?.email}</h4><br/>

                    <label>Phone</label><br/>
                    <h4>{customer?.phone}</h4><br/>

                    <label>Address</label><br/>
                    <h4>{customer?.attendant}</h4>
                    </div>

                  </div>
            }
        </>
    )
}