import React , { useEffect, useReducer } from "react";
import { fetchData } from "../commons/fetch-data";
import {CheckCircleTwoTone, FieldTimeOutlined,FundOutlined } from '@ant-design/icons';
import  {CardInfo} from '../models/cardInfo';

interface CardInfoDetailState{
    cardInfo: CardInfo | null;
    loading: boolean;
}

interface ActionTypes{
    readonly type: 'GET_CARD' | 'GET_CARD_SUCCESS' | 'GET_CARD_FAILED';
    payload?: CardInfo;
}

function reducer(state: CardInfoDetailState, action: ActionTypes)
{
    switch (action.type){
        case 'GET_CARD':
            return{
                ...state,
                loading: true
            };
        case 'GET_CARD_SUCCESS':
            return{
                ...state,
                loading: false,
                cardInfo: action.payload!
            };
        case 'GET_CARD_FAILED':
            return{
                ...state,
                loading: false
            };
        default:
            throw new Error('Unknow action');
    }
    
}

export function CardItems() {
    const [state, dispatch] = useReducer(reducer, {
        cardInfo: null,
        loading: false
    });
    const {cardInfo, loading} = state;
    useEffect(() => {        
        dispatch({
            type: 'GET_CARD'
        });
        (async () => {
            try {
                const response = await fetchData('http://localhost:4010/cardinfo');
                const data = await response.json() as CardInfo;
                console.log(data);
                dispatch({
                    type: 'GET_CARD_SUCCESS',
                    payload: data
                });
            } 
            catch (error) {
                dispatch({
                    type: 'GET_CARD_FAILED'
                });
            }            
        })();
    }, []);
    return(
        <>
        {
            loading ? <p>Loading...</p>
                    : <div className="cards">
                    <div className="card-item">
                        <div className="card-icon">
                            <CheckCircleTwoTone twoToneColor="#52c41a" style={{fontSize: 32}}></CheckCircleTwoTone>                            
                        </div>
                        <div className="card-info">
        <div className='card-main-text'>{cardInfo?.resolvedTicket.toLocaleString('en')}</div>
                            {/* <div>Resloved tickets</div> */}
                        </div>
                    </div>
                    <div className="card-item">
                        <div className="card-icon">
                            <FieldTimeOutlined twoToneColor="#d02dee" style={{fontSize: 32}}></FieldTimeOutlined>                            
                        </div>
                        <div className="card-info">
                            <div className='card-main-text'>{cardInfo?.timeAverage.toLocaleString('en')}m</div>
                            {/* <div>Average response</div> */}
                        </div>
                    </div>
                    <div className="card-item">
                    <div className="card-icon">
                        <FundOutlined twoToneColor="#52c41a" style={{fontSize: 32}}></FundOutlined>                            
                        </div>
                        <div className="card-info">
                            <div className='card-main-text'>{cardInfo?.medianNPS.toLocaleString('en')}</div>
                            {/* <div>Median NPS</div> */}
                        </div>
                    </div>
                </div>
        }
        </>
    );
}